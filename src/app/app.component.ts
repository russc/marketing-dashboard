import { Component } from '@angular/core';
import {
  newLayoutResponse,
  backendDataResponse,
} from 'dashboard-mock-response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = newLayoutResponse.displayName;
  fields = newLayoutResponse.fieldDefinitions;
  layout = newLayoutResponse.layout;
  points = backendDataResponse.dataPoints;
  dataSets = backendDataResponse.dataSets;
}
