import { Component, Input } from '@angular/core';

import type {
  DatasetResponse,
  FieldDefinitions,
  Element,
} from 'dashboard-mock-response';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss'],
})
export class DataGridComponent {
  @Input() fields: FieldDefinitions | undefined;
  @Input() element: Element | undefined;
  @Input() dataSets: DatasetResponse[] | undefined;
  @Input() label: string = '';
  gridData: DatasetResponse | undefined;

  ngOnChanges() {
    const current = this.dataSets?.find(
      (item) => item.name === this.element?.name
    );

    this.gridData = current;
  }
}
