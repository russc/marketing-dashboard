import { Component, Input } from '@angular/core';

import type { FieldDefinitions, DataResponse } from 'dashboard-mock-response';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() fields: FieldDefinitions | undefined;
  @Input() points: DataResponse | undefined;
  @Input() element: any;
}
