import { Pipe, PipeTransform } from '@angular/core';
import {
  formatCurrency,
  getCurrencySymbol,
  formatNumber,
  formatPercent,
} from '@angular/common';
import type { DataResponse, Format } from 'dashboard-mock-response';

interface Agg {
  method: 'average' | 'sum' | 'none';
  data: DataResponse[];
  footerIndex: number;
  field: string;
}

@Pipe({
  name: 'datum',
})
export class DatumPipe implements PipeTransform {
  locale = 'en-US';
  aggregate(val: number | string, agg: Agg) {
    const sum: number = agg.data.reduce((acc: number, cur: any) => {
      return acc + (cur[val] as number);
    }, 0);

    switch (agg.method) {
      case 'average':
        return sum / agg.data.length;
      case 'sum':
        return sum;
      case 'none':
        return val;
    }
  }

  transform(
    value: number | string,
    format: Format,
    digitsInfo?: string,
    agg?: Agg
  ): number | string {
    if (typeof value === 'number' || agg) {
      const raw: number | string | undefined = agg
        ? this.aggregate(value, agg)
        : value;

      switch (format) {
        case 'currency':
          return formatCurrency(
            raw as number,
            'en-US',
            getCurrencySymbol('USD', 'narrow'),
            'USD',
            digitsInfo
          );
        case 'percent':
          if (agg) {
            return '-';
          }
          return formatPercent(raw as number, this.locale, digitsInfo);

        case 'number':
          return formatNumber(raw as number, this.locale, digitsInfo);
        case 'none':
          if (agg) {
            return agg.footerIndex === 0 ? 'Total' : '';
          }
          return '-';
        default:
      }
    }

    return value;
  }
}
